# Red Alert

A simple Go program to send telegram messages when certain posts are posted a subreddit.

For example, when ram is posted to /r/buildapcsales, when information about your favorite game is posted to /r/games, or when someone posts about a restaurant in your city.

This was mainly made as an exercise to use channels and interfaces, two Go features I have not touched, as well as using a chatbot for notifications

## Configuration

See config.json.example. Remove example after setting it up.

Reddit:
  - Register an app on your account to receive a secret and ID annd udpate config.json
  - Optionally put a user to notify if using reddit for notifications

Telegram:
  - Register a bot token and get the ID of the room with yourself
  - Token: https://telegram.me/botfather
  - Room ID - https://stackoverflow.com/questions/32423837/telegram-bot-how-to-get-a-group-chat-id

PushBullet:
  - Get an api token from your account
  - Optionally enter a device ID to send pushes to, or a channel ID to send pushed to
  - https://www.pushbullet.com/#settings/account

### Filtering

Also needed is a map of subreddits and prices/regex to match
Going off the example in config.json, whenever a post is submitted to /r/buildapcsales the title is scanned for "ram" and "8TB". If either of these match, a notification is sent with a link to the post 

To set up a filter, add one in config.json
Here's an example on how to set them.
```jsons
...
  "Watch" : {
       "buildapcsales": ["8tb", "[RAM];16", "[GPU];580;<$200",`rx ?590`],
       "frugalmalefashion": ["J.Crew", "free", "killshot"],
...
```
Every filter is used as regex, unless it contains a price. So "8tb" is actually just a regex matching any title with "8tb".

In this example, buildapacsales and frugalemalefashion are both monitored.
The following titles will kick off a notification for buildapcsales
- Any title with ```8tb```
- Any title with both the ```RAM``` tag and ```16``` somewhere
- Any title with ```GPU```, ```580```, and a price lower than $200
- Any title with ```rx 590``` or ```rx590```

#### Complex Regex

To use complex regex use back ticks, ``, in the entry  

#### Multiple restrictions

You can require multiple matches by using ```;```. For example, 
 ```"buildapcsales": ["8tb", "[RAM];16"]``` will send a notification if the title has ```[RAM]``` and ```16``` in the title OR ```8tb```.

#### Prices

Basic price filtering works. For example, 
 ```"buildapcsales": ["GPU;<$100"]``` will only send a notification if the title has both ```GPU``` in the title and a price lower than ```100```. ```<,>,=``` comparisons all work.

## Running

```
./red-alert -h
Usage of ./red-alert:
  -sub string
      Service to use for pushing events { telegram |  pushbullet | reddit } (default "telegram")
```

The telegram bot has two simple commands right now. Enter "/pause" to pause notifications, and enter "/unpause" to start them again

## Extending

Other bots/services can be used than telegram and pushbullet, they must implement a "sub" method to subscribe to new reddit posts, and a "push" method to send the notification