package alerter

import (
	"fmt"
	"log"
	"os"

	"github.com/turnage/graw/reddit"
)

// Loggers
var (
	infolog = log.New(os.Stderr, "[INFO] ", log.Ltime)
	errlog  = log.New(os.Stderr, "[ERROR] ", log.Ltime)
)

// Bot interface for sending messages
type notifier interface {
	Push(string, string) error
}

// Hold our bots, our queue of posts, and our filter
type Alerter struct {
	Bots   map[string]notifier
	Posts  <-chan *reddit.Post
	Filter func(reddit.Post) bool
}

// Creates a new manager
func AlertManager() Alerter {
	var a Alerter
	a.Bots = make(map[string]notifier)
	return a
}

// Adds notifier to the manager
func (a *Alerter) Add(name string, b notifier, queue <-chan *reddit.Post, fn func(reddit.Post) bool) {
	a.Bots[name] = b
	a.Posts = queue
	a.Filter = fn
}

// Waits for a message in the channel, then pushes it, forever
func (a *Alerter) Start(bot string) {
	m := fmt.Sprintf("Starting %s notifier", bot)
	infolog.Print(m)
	a.Bots[bot].Push(m, " ")
	for {
		select {
		case post := <-a.Posts:
			if ok := a.Filter(*post); ok {
				link := "https://reddit.com" + post.Permalink
				infolog.Print("Sending message")
				if err := a.Bots[bot].Push(post.Title, link); err != nil {
					errlog.Print(err)
				}
			}
		}
	}
}
