FROM golang:1.12.7-alpine

ADD . /proj

RUN apk update \
 && apk add git gcc musl-dev ca-certificates \
 && cd /proj \
 && go get \
 && go build -o ./run

FROM alpine:3.9

COPY --from=0 /proj/run /proj/run
COPY --from=0 /proj/config.json /proj/config.json

# Because we need to use oath (https), we need our previous cert
COPY --from=0 /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt

WORKDIR /proj

ENTRYPOINT ["/proj/run"]




