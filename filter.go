package main

import (
	"regexp"
	"strconv"
	"strings"

	"github.com/turnage/graw/reddit"
)

// Struct for our filter
type Filter struct {
	Subreddits map[string][]string
}

// FilterTile Matches our config entries to a reddit title
// No regex for now, plan to add that in the future if this works "okay"
func (f *Filter) FilterTitle(post reddit.Post) bool {
	for _, substring := range f.Subreddits[post.Subreddit] {
		terms := strings.Split(substring, ";")
		isGood := true
		// We want to make sure every term is a match, so set to to false if any fail
		for _, term := range terms {
			match := filter(post.Title, term)
			if !match {
				isGood = false
			}
		}
		if isGood {
			return true
		}
	}
	return false
}

// Our actual filter
// Prices are checked first. If there are no prices, then we assume the filter is regex and
// check it
func filter(title string, substring string) bool {
	title = strings.ToLower(title)
	substring = strings.ToLower(substring)
	isPrice, prices := isPrice(title)
	switch {
	case substring == "*":
		return true
	case strings.HasPrefix(substring, "<") && isPrice:
		cStripped := strings.Trim(substring, "<$")
		price, _ := strconv.Atoi(cStripped)
		for _, p := range prices {
			if p < price {
				return true
			}
		}
	case strings.HasPrefix(substring, ">") && isPrice:
		cStripped := strings.Trim(substring, ">$")
		price, _ := strconv.Atoi(cStripped)
		for _, p := range prices {
			if p > price {
				return true
			}
		}
	case strings.HasPrefix(substring, "=") && isPrice:
		cStripped := strings.Trim(substring, "=$")
		price, _ := strconv.Atoi(cStripped)
		for _, p := range prices {
			if p == price {
				return true
			}
		}
	default:
		match, _ := regexp.MatchString(substring, strings.ToLower(title))
		if match {
			return true
		}
	}
	return false
}

// Find prices in a title
func isPrice(title string) (bool, []int) {
	var prices []int
	var validID = regexp.MustCompile(`\$\d+(?:\.\d+)?`)
	matches := validID.FindAllString(title, -1)
	for x := range matches {
		price := matches[x]
		price = strings.Trim(price, "$")
		if price, err := strconv.Atoi(price); err == nil {
			prices = append(prices, price)
		}
	}
	if len(prices) > 0 {
		return true, prices
	}
	return false, prices
}
