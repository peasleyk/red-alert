package main

import (
	"testing"

	"github.com/turnage/graw/reddit"
)

var (
	f = &Filter{}
)

func TestMain(m *testing.M) {
	var subs = make(map[string][]string)
	subs["sub1"] = []string{"term1", "term2"}
	subs["sub2"] = []string{"Elite"}
	subs["sub3"] = []string{"sale", "live"}
	subs["sub4"] = []string{"*"}
	subs["sub5"] = []string{"<$100"}
	subs["sub6"] = []string{">$100"}
	subs["sub7"] = []string{"=100"}
	subs["sub8"] = []string{"must contain;>$100", "or this"}
	subs["sub9"] = []string{`rx ?580`}
	subs["sub10"] = []string{"RAM;16"}

	f.Subreddits = subs
}

func TestFilterSpaced(t *testing.T) {
	redditPost := reddit.Post{
		Title:     "term1 term2 ",
		Subreddit: "sub1",
	}

	if ok := f.FilterTitle(redditPost); !ok {
		t.Error("title contains term1")
	}
}

func TestFilterContains(t *testing.T) {
	redditPost := reddit.Post{
		Title:     "term2 term3",
		Subreddit: "sub1",
	}

	if ok := f.FilterTitle(redditPost); !ok {
		t.Error("title contains term2")
	}
}

func TestFilterAnySubAnyPost(t *testing.T) {
	redditPost := reddit.Post{
		Title:     "any title",
		Subreddit: "sub4",
	}
	if ok := f.FilterTitle(redditPost); !ok {
		t.Error("title is anything")
	}
}

func TestPriceMore(t *testing.T) {
	redditPost := reddit.Post{
		Title:     "item price$110. sale",
		Subreddit: "sub5",
	}
	if ok := f.FilterTitle(redditPost); ok {
		t.Error("110 is more than 100")
	}
}

func TestPriceLess(t *testing.T) {
	redditPost := reddit.Post{
		Title:     "r $190",
		Subreddit: "sub6",
	}
	if ok := f.FilterTitle(redditPost); !ok {
		t.Error("190 is more than 100")
	}
}

func TestPriceEquals(t *testing.T) {
	redditPost := reddit.Post{
		Title:     "for sale $100.",
		Subreddit: "sub7",
	}
	if ok := f.FilterTitle(redditPost); !ok {
		t.Error("100 equals 100")
	}
}

func TestPriceMultiple(t *testing.T) {
	redditPost := reddit.Post{
		Title:     "must contain a price $150",
		Subreddit: "sub8",
	}
	if ok := f.FilterTitle(redditPost); !ok {
		t.Error("Should have matched all")
	}
}

func TestPriceMultipleFail(t *testing.T) {
	redditPost := reddit.Post{
		Title:     "must contain a price $90",
		Subreddit: "sub8",
	}
	if ok := f.FilterTitle(redditPost); ok {
		t.Error("Should have failed at $90")
	}
}

func TestRegex(t *testing.T) {
	redditPost := reddit.Post{
		Title:     "rx580",
		Subreddit: "sub9",
	}

	if ok := f.FilterTitle(redditPost); !ok {
		t.Error("title contains rx580")
	}
}

func TestMatchBoth(t *testing.T) {
	redditPost := reddit.Post{
		Title:     "[CPU] Intel Core i9-9900K Coffee Lake 8-Core, 16-Thread, 3.6 GHz (5.0 GHz Turbo) 474.99$ PROMO:EMCTYUD24",
		Subreddit: "sub10",
	}
	if ok := f.FilterTitle(redditPost); ok {
		t.Error("Should have failed at with no [RAM]")
	}
}
