module gitlab.com/peasleyk/red-alert

require (
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/kylelemons/godebug v0.0.0-20170820004349-d65d576e9348 // indirect
	github.com/mitchellh/mapstructure v1.1.2 // indirect
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	github.com/turnage/graw v0.0.0-20180517193449-15877a6c18d6
	github.com/turnage/redditproto v0.0.0-20151223012412-afedf1b6eddb // indirect
	golang.org/x/oauth2 v0.0.0-20181203162652-d668ce993890 // indirect
	golang.org/x/tools v0.0.0-20190530215528-75312fb06703 // indirect
	google.golang.org/appengine v1.5.0 // indirect
)
