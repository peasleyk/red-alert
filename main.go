package main

import (
	"encoding/json"
	"flag"
	"log"
	"os"

	. "gitlab.com/peasleyk/red-alert/alerter"

	"github.com/turnage/graw"
	"github.com/turnage/graw/reddit"
	bot "gitlab.com/peasleyk/red-alert/bots"
)

// Configuration file definitions
type configuration struct {
	Telegram struct {
		Token   string
		GroupID int64
	}
	Reddit struct {
		Username string
		Password string
		Secret   string
		ID       string
		Notify   string
	}
	Subreddits map[string][]string
	PushBullet struct {
		Token      string
		ChannelTag string
		DeviceID   string
	}
	Gotify struct {
		Token string
		Url   string
	}
}

// Loggers
var (
	infolog = log.New(os.Stderr, "[INFO] ", log.Ltime)
	errlog  = log.New(os.Stderr, "[ERROR] ", log.Ltime)
)

// Runs through the configuration file to decode settings
func loadConfig(location string) (configuration, error) {
	configuration := *new(configuration)
	file, err := os.Open(location)
	defer file.Close()
	if err != nil {
		return configuration, err
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&configuration)
	if err != nil {
		return configuration, err
	}
	return configuration, nil
}

func main() {
	var config, err = loadConfig("config.json")
	if err != nil {
		errlog.Print(err)
		os.Exit(1)
	}
	notifier := flag.String("sub", "telegram", "Service to use for pushing events { telegram |  pushbullet | reddit | gotify }")
	flag.Parse()

	infolog.Print("Setting up bots")
	var postQueue = make(chan *reddit.Post)
	rBot := bot.NewRedditBot(config.Reddit.ID,
		config.Reddit.Secret,
		config.Reddit.Username,
		config.Reddit.Password)
	streamsCfg := rBot.NewStreamsConfig(&config.Subreddits)
	_, wait, err := graw.Run(&bot.Announcer{Queue: postQueue}, rBot.GrawBot, streamsCfg)
	if err != nil {
		errlog.Print(err)
		os.Exit(1)
	}

	f := Filter{}
	f.Subreddits = config.Subreddits
	a := AlertManager()
	switch *notifier {
	case "telegram":
		a.Add("telegram",
			bot.NewTelegramBot(config.Telegram.GroupID, config.Telegram.Token),
			postQueue,
			f.FilterTitle)
		a.Start("telegram")
	case "pushbullet":
		a.Add("pushbullet",
			bot.NewPushBulletBot(config.PushBullet.Token,
				config.PushBullet.ChannelTag,
				config.PushBullet.DeviceID),
			postQueue,
			f.FilterTitle)
		a.Start("pushbullet")
	case "gotify":
		a.Add("gotify",
			bot.NewGotifyBot(config.Gotify.Token,
				config.Gotify.Url),
			postQueue,
			f.FilterTitle)
		a.Start("gotify")
	case "reddit":
		rBot.User = config.Reddit.Username
		a.Add("reddit",
			rBot,
			postQueue,
			f.FilterTitle)
		a.Start("reddit")

	}

	// Run this last to allow bots routine to start before blocking
	if err := wait(); err != nil {
		errlog.Printf("[REDDIT]  %s", err)
		os.Exit(1)
	}
}
