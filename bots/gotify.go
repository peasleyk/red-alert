package bot

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

type gotify struct {
	token string
	url   string
}

type gotifyBody struct {
	Title    string `json:"title"`
	Priority int    `json:"priority"`
	Message  string `json:"message"`
}

//NewGotifyBot Creates a not gotify bot
func NewGotifyBot(token string, url string) gotify {
	b := gotify{
		token: token,
		url:   url,
	}
	return b
}

// Interacts with gotify api to send a push notification
func (b gotify) Push(title string, message string) error {
	body := gotifyBody{
		Title:   title,
		Message: message,
	}

	jBody, err := json.Marshal(body)
	if err != nil {
		return err
	}

	client := &http.Client{}
	fullURL := b.url + b.token
	req, err := http.NewRequest("POST", fullURL, bytes.NewBuffer(jBody))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	if res.StatusCode > 300 {
		body, _ := ioutil.ReadAll(res.Body)
		return errors.New(string(body))
	}
	if res != nil {
		defer res.Body.Close()
	}

	return nil
}
