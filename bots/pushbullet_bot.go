package bot

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

const (
	pbURL = "https://api.pushbullet.com/v2/pushes"
)

type pushBullet struct {
	token   string
	channel string
	pushUrl string
	device  string
}

type pushbulletBody struct {
	Title    string `json:"title"`
	Body     string `json:"body"`
	PushType string `json:"type"`
	Channel  string `json:"channel_tag"`
	DeviceID string `json:"device_iden"`
}

func NewPushBulletBot(token string, channel string, device string) pushBullet {
	b := pushBullet{
		token:   token,
		pushUrl: pbURL,
		channel: channel,
		device:  device,
	}
	return b
}

// Interacts with pushbullet api to send a push notification
func (b pushBullet) Push(title string, messages string) error {
	body := pushbulletBody{
		Title:    title,
		Body:     messages,
		PushType: "link",
		Channel:  b.channel,
		DeviceID: b.device,
	}

	jBody, err := json.Marshal(body)
	if err != nil {
		return err
	}

	client := &http.Client{}
	req, err := http.NewRequest("POST", b.pushUrl, bytes.NewBuffer(jBody))
	if err != nil {
		return err
	}
	req.Header.Set("Access-Token", b.token)
	req.Header.Set("Content-Type", "application/json")
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	if res.StatusCode > 300 {
		body, _ := ioutil.ReadAll(res.Body)
		return errors.New(string(body))
	}
	if res != nil {
		defer res.Body.Close()
	}

	return nil
}
