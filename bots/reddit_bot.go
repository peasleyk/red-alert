package bot

import (
	"github.com/turnage/graw"
	"github.com/turnage/graw/reddit"
)

// Both acts as a pusher and a subscriber
type wrappedRbot struct {
	GrawBot reddit.Bot
	User    string
}

func NewRedditBot(ID string, secret string, username string, password string) wrappedRbot {
	redditCfg := reddit.BotConfig{
		Agent: "notification bot",
		App: reddit.App{
			ID:       ID,
			Secret:   secret,
			Username: username,
			Password: password,
		},
		Rate: 10,
	}
	GrawBot, err := reddit.NewBot(redditCfg)
	if err != nil {
		panic("Reddit error: " + err.Error())
	}
	b := wrappedRbot{
		GrawBot: GrawBot,
	}
	return b
}

// Runs through the subs we want to watch so we can monitor them
func (b *wrappedRbot) NewStreamsConfig(Watch *map[string][]string) graw.Config {
	var subreddits []string
	for k := range *Watch {
		subreddits = append(subreddits, k)
	}
	streamsCfg := graw.Config{
		Subreddits: subreddits,
	}
	return streamsCfg
}

// Sends itself a message
func (b wrappedRbot) Push(title string, message string) error {
	if err := b.GrawBot.SendMessage(b.User, title, message); err != nil {
		return err
	}
	return nil
}

// Ingests new reddit posts, or "announces" then
type Announcer struct {
	Queue chan<- *reddit.Post
}

// New posts are continually fed into a queue
// There are not processed here so we don't miss new posts
func (a *Announcer) Post(post *reddit.Post) error {
	a.Queue <- post
	return nil
}
