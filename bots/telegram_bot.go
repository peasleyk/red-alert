package bot

import (
	"log"
	"os"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type wrappedTbot struct {
	bot     *tgbotapi.BotAPI
	groupID int64
	pause   bool
	log     *log.Logger
}

// Returns a wrapped telegram bot that allows us to extend it
func NewTelegramBot(groupID int64, token string) wrappedTbot {
	tBot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		panic("Telegram error: " + err.Error())
	}
	b := wrappedTbot{
		bot:     tBot,
		groupID: groupID,
		log:     log.New(os.Stderr, "[TELEGRAM] ", log.Ltime|log.Lshortfile),
	}
	go b.start()
	return b
}

// Send a messages to the channel
func (b wrappedTbot) Push(title string, message string) error {
	msgfmt := title + " " + message
	msg := tgbotapi.NewMessage(b.groupID, msgfmt)
	b.bot.Send(msg)
	return nil
}

// Simple commands to pause and unpause for now,
// need to write titles to DB when paused for the future
// and have method of getting them
func (b *wrappedTbot) start() {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 120
	updates, err := b.bot.GetUpdatesChan(u)
	if err != nil {
		b.log.Print(err)
	}
	b.log.Print("Starting telegram listener")
	for {
		select {
		case message := <-updates:
			if message.Message.IsCommand() {
				b.log.Print("Received command: " + message.Message.Text)
				switch message.Message.Text {
				case "/pause":
					b.pause = true
					b.Push("", "Notifications paused")
				case "/unpause":
					b.pause = false
					b.Push("", "Notifications unpaused")
				}
			}
		}
	}
}
